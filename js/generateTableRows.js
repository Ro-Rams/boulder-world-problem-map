$(document).ready(function() {
    $("rect").click(elementId)
    $("polygon").click(elementId)
})
var elementId = function(e) {
    console.log($(this).attr('id'))
    $("#route-info-table #from-db").remove()
    $.ajax({
        type: "GET",
        url: "display.php",
        dataType: "json",
        success: function(response) {
            for (i = 0; i < response.length; i++) {
		var id = i+1
                var row = response[i];
                var active = (row.Active == 1) ? true : false
                $("#route-info-table").append(
                    "<tr id='from-db'><td>" + id + ' ' + row.Route_Difficulty
                    + "</td><td>" + row.Crux_Type
                    + "</td><td>" + row.Setter_Name
                    + "</td><td>" + active
                    + "</td></tr>"
                )
            }
        }
    })
    $("#route-info-table").scrollView();
}
$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 1000);
    });
}
